Source: libfile-read-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Damyan Ivanov <dmn@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: perl,
                     libfile-slurp-perl,
                     libtest-pod-perl,
                     libtest-pod-coverage-perl,
                     libtest-distribution-perl,
                     libtest-portability-files-perl
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libfile-read-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libfile-read-perl.git
Homepage: https://metacpan.org/release/File-Read

Package: libfile-read-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libfile-slurp-perl
Multi-Arch: foreign
Description: interface for reading one or more files
 File::Read mainly proposes functions for reading one or more files, with
 different options.
 .
 This module was created to address a quite specific need: reading many files,
 some as a normal user and others as root, and eventually do a little more
 processing, all while being at the same time compatible with Perl 5.004.
 File::Slurp addresses the first point, but not the others, hence the creation
 of File::Read. If you don't need reading files as root or the post-processing
 features, then it's faster to directly use File::Slurp.
